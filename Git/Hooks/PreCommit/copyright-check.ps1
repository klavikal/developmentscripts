$rootDirectory = Split-Path -Parent $MyInvocation.MyCommand.Path
$srcRoot = (get-item $rootDirectory).parent.parent.parent.FullName

# Write-Host Root: $rootDirectory
# Write-Host Src: $srcRoot

$files = (git diff HEAD --name-only -- '*.cs') | 
    Where-Object {Test-Path -Path $_ -PathType Leaf -Exclude *.Designer.cs } |
    Foreach-Object {Join-Path -Path $srcRoot -ChildPath $_}
    

Write-Host "Files: $($files.Count)"

if($($files.length) -eq 0)
{
    return
}

$copyrightRegex = "// Copyright.+$((Get-Date).year)"

foreach($file in $files)
{
    $header = gc $file | select -first 10
    $hasCopyright = $header -match $copyrightRegex
    
    if($hasCopyright.count -eq 0){
        Write-Host "$file does not meet copyright style" -ForegroundColor Red
        exit 1
    }
}

Write-Host "Copyright checks passed!" -ForegroundColor Green