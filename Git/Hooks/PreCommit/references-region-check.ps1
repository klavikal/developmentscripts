$rootDirectory = Split-Path -Parent $MyInvocation.MyCommand.Path
$srcRoot = (get-item $rootDirectory ).parent.parent.parent.FullName

#Write-Host Root: $srcRoot

$files =  $(git diff HEAD --name-only) | Foreach-Object {Join-Path -Path $srcRoot -ChildPath $_} 
$usingStatementsRegex = "^using.+;$"

Write-Host "Files: $($files.Count)"

if($($files.length) -eq 0)
{
    return
}

foreach($file in $files)
{
    $header = gc $file | select -first 20
    $hasUsings = $header -match $usingStatementsRegex
    
    if($hasUsings.count -gt 0){

        $hasRegion = $header -match "^#region\sReferences\s*$"
        if($hasRegion.count -eq 0)
        {
            Write-Host "$file does not contain a References region around using statements" -ForegroundColor Red
            exit 1
        }        
    }
}

Write-Host "References Region checks passed!" -ForegroundColor Green



