if [ -z "$1" ]
then
    echo "Argument empty. Aborting"
    exit
fi

git branch | grep -i "$1"

echo "These results will be deleted. Proceed? (y/n)"
read proceedValue

if [ "$proceedValue" == "y" ]
then
    echo "Removing local branches..."
    git branch | grep -i "$1" | xargs git branch -D
elif [ -z "$proceedValue" ]
then
    echo "No input value. Aborting"
else
    echo "Not removing any branches"
fi    

