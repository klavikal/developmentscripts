if [ -z "$1" ]
then
    echo "Argument empty. Aborting"
    exit
fi


git checkout -b $1
git push -u origin $1
git status -sb