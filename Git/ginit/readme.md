this is a helpful tool to set up a new branch. the string you give
as the argument will be used to create the new branch as well as the 
initial push to create a matching remote branch it's linked to.

As a sanity check it ends with printing git status so you can verify
initialization went right