SET slnPath=""
SET workingDir=%~dp0

msbuild /m -v:n -fileLogger -flp:logfile=%workingDir%buildSlnDebug.log "%slnPath%" /p:Configuration=Debug /p:Platform="Any CPU"
msbuild /m -v:n -fileLogger -flp:logfile=%workingDir%buildSlnRelease.log "%slnPath%" /p:Configuration=Release /p:Platform="Any CPU"
pause