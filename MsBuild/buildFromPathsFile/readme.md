This script will build solutions from a file that contains a list of sln paths in order listed in the provided file.

Example paths file format:

```
"C:\Folder1\Folder2\Folder3\SolutionA.sln"
"C:\Folder1\Folder2\Folder3\Folder4\SolutionB.sln"
"C:\Folder1\Folder2\Folder3\Folder5\Folder6\SolutionC.sln"
```

this can be fed into the script by calling
`.\buildSlnsFromPathList.ps1 -slnPathsFile:"{Path to file containing sln paths}"`

this builds them in debug configuration by default but allows also building release by using the `-buildRelease:$true` argument

This will run the build command again on the same solution using the release configuration.

This script puts the msbuild output into files located at:
`{powershell script directory}\buildResults`

This will have a folder called `Debug` with files names `{solutionName}.log` as well as a matching format in the `Release` folder if the release configuration was used.

A helpful use case for this script would be to make separate powershell script(s) that call this one and passes in the file path(s) and configuration arg(s). For example there could be two powrshell scripts each calling the building script and providing the file path applicable to the target project and the build configuration.  This would allow you to set these helper scripts to shortcuts or a shortcut utility such as slickrun etc.

