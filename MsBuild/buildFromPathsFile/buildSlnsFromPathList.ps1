param (
    [string]$slnPathsFile = "",
    [switch]$buildRelease,
    [string]$buildConfig = "Debug",
    [switch]$clean
)

function PrintBoxedText {
    param (
        [string]$Text
    )
	$textLength = $Text.Length
	$boxChar = "="
	$boxLine = $boxChar.PadRight($textLength+3,$boxChar)
	
    Write-Host  $boxLine -ForegroundColor Green
    Write-Host $Text -ForegroundColor Green
    Write-Host  $boxLine -ForegroundColor Green
    
}

function BuildSln {
    param (
        [string]$slnPath,
        [string]$logFilePath,
        [string]$buildConfig,
        [switch]$clean 
    )

    # Write-Host "SLN: $slnPath"
    # Write-Host "LOG: $logFilePath"
    # Write-Host "CFG: $buildConfig"

    #nuget restore $slnPath
    if($clean)
    {
        PrintBoxedText -Text "Cleaning $slnPath ($buildConfig)"
        msbuild /m -clp:ErrorsOnly -clp:Summary -nologo -v:q "$slnPath" /t:clean /p:Configuration=$buildConfig /p:Platform="Mixed Platforms"
    }

    PrintBoxedText -Text "Building $slnPath ($buildConfig)"
    msbuild /m -clp:ErrorsOnly -clp:Summary -nologo -v:q -fileLogger -flp:logfile=$logFilePath "$slnPath" /p:Configuration=$buildConfig /p:Platform="Mixed Platforms"
}

function GetLogPath {
    param (
        [string]$buildConfig,
        [string]$slnName
    )

    return "$PSScriptRoot\buildResults\$buildConfig\$slnName.log"   
}

foreach($line in Get-Content $slnPathsFile){
    
    $slnName = Split-Path $line -leaf
    $slnName = $slnName -replace '"','' 
    # $buildConfig = "Debug"
    $logFile = GetLogPath -buildConfig $buildConfig -slnName $slnName

    Write-Host

    if($buildRelease)
    {
        $buildConfig = "Release"
        $logFile = GetLogPath -buildConfig $buildConfig -slnName $slnName
    }

    BuildSln -slnPath $line -logFilePath $logFile -buildConfig $buildConfig -clean:$clean
}

PrintBoxedText -Text "Complete!!"